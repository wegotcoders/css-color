# Color Palette CSS Color Practice #

Using CSS to add color to elements in a HTML document.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a HTML document and CSS style sheets.
2. Add your style rules to style.css, this file is linked to from color-palette.html.
3. Open a tab in your browser and go to http://paletton.com/, you will be using this to create your color palette.
4. Open color-palette.html by typing in terminal 'open color-palette.html', you are going to add color to this.
5. Add declarations to change the background color of each element in the HTML document to reflect the palette you have created with Paletton.
6. You can see your progress by typing in terminal 'open color-palette.html'.

(The declarations you wrote in style.css overrode the ones found in box-styles.css because of the order they are loaded in.)
